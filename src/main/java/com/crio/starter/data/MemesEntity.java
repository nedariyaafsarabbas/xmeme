package com.crio.starter.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import java.util.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "memes")
public class MemesEntity {

    @Transient
    public static final String SEQUENCE_NAME = "users_sequence";

    @Id
    private String id;

    @NonNull
    private String name;

    @NonNull
    private String url;

    @NonNull
    private String caption;

    @Override
    public boolean equals(Object o){
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemesEntity entity = (MemesEntity) o;
        return Objects.equals(name , entity.name) 
                && Objects.equals(url, entity.url) 
                && Objects.equals(caption, entity.caption);
    }

    public boolean validateNull(){
        if(this.name == null || this.url == null || this.caption == null){
            return true;
        }
        return false;
    }
}
