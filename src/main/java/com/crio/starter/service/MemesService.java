package com.crio.starter.service;

import lombok.RequiredArgsConstructor;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.crio.starter.data.DatabaseSequence;
import com.crio.starter.data.MemesEntity;
import com.crio.starter.repository.MemesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemesService {

    private final MemesRepository memesRepository;
    
    @Autowired
	private MongoOperations mongoOperation;

    public String generateSequence(String seqName) {

        Query query = new Query(Criteria.where("_id").is(seqName));
        Update update = new Update();
	    update.inc("seq", 1);
        FindAndModifyOptions options = new FindAndModifyOptions();
	    options.returnNew(true).upsert(true);
        

        DatabaseSequence counter = mongoOperation.findAndModify(query,update,options , DatabaseSequence.class);
        return !Objects.isNull(counter) ? String.valueOf(counter.getSeq()) : "1";
    }

    public List<MemesEntity> findAllMemes(){
        List<MemesEntity> data = memesRepository.findAll();
        if(data.size() <= 100){
            return data;
        }
        else{
            List<MemesEntity> temp_data = new ArrayList<>();
            data.sort(Comparator.comparing(entity -> Long.parseLong((entity).getId()), Comparator.reverseOrder()));
            for(int i = 0; i < 100; i++){
                temp_data.add(data.get(i));
            }   
            return temp_data;
        }
        
    }

    public MemesEntity save(MemesEntity memesEntity){
        memesEntity.setId(generateSequence(MemesEntity.SEQUENCE_NAME));
        memesRepository.save(memesEntity);
        return memesEntity;
    }

    public Optional<MemesEntity> findById(String id){
        return memesRepository.findById(id);
    }

    public boolean checkDataExist(MemesEntity entity){
        List<MemesEntity> allMemes = memesRepository.findAll();
        for(MemesEntity m : allMemes){
             if(m.equals(entity)){
                 return true;
             }
        }
        return false;
    }
    
}
