package com.crio.starter.controller;

import lombok.RequiredArgsConstructor;
import java.util.List;
import java.util.Optional;
import com.crio.starter.data.MemesEntity;
import com.crio.starter.exchange.MemesResponse;
import com.crio.starter.service.MemesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MemesController {
    
    private final MemesService memesService;

    
    @GetMapping("/memes")
    public List<MemesEntity> allMemes(){
        return memesService.findAllMemes();
    }
    @GetMapping("/memes/{id}")
    public ResponseEntity<MemesEntity> findById(@PathVariable String id){
        Optional<MemesEntity> mOptional = memesService.findById(id);
        if(mOptional.isPresent()){
            MemesEntity memesEntity = mOptional.get();
            return new ResponseEntity<MemesEntity>(memesEntity, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    @PostMapping("/memes")
    public ResponseEntity<MemesResponse> saveData(@RequestBody MemesEntity memesEntity){
        if(memesEntity.validateNull()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(memesService.checkDataExist(memesEntity)){
             return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        MemesEntity tempData = memesService.save(memesEntity);
        MemesResponse memesResponse = new MemesResponse(String.valueOf(tempData.getId()));
        return new ResponseEntity<MemesResponse>(memesResponse , HttpStatus.OK);
    }

}
