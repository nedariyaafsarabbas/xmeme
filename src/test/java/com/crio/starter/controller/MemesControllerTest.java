package com.crio.starter.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import com.crio.starter.data.MemesEntity;
import com.crio.starter.exchange.ResponseDto;
import com.crio.starter.service.GreetingsService;
import com.crio.starter.service.MemesService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

@AutoConfigureMockMvc
@SpringBootTest
public class MemesControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    MemesService memesService;

    @Test
    void getMemesbyIdTest() throws Exception{
        MemesEntity entity = new MemesEntity("10", "Afsar Abbas", "http://test.com/url", "This is test data");

        Mockito.doReturn(Optional.of(entity))
            .when(memesService).findById("10");
        
        URI uri = UriComponentsBuilder
            .fromPath("/memes/10")
            .build().toUri();
        
        MockHttpServletResponse response = mvc.perform(
                get(uri.toString()).accept(APPLICATION_JSON_VALUE)
            ).andReturn().getResponse();
        
        String responeJson = response.getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        MemesEntity entity2 = objectMapper.readValue(responeJson, MemesEntity.class);

        assertEquals(entity, entity2);

        Mockito.verify(memesService, Mockito.times(1)).findById("10");
    }

    @Test
    void getAllMemes() throws Exception{
        MemesEntity entity = new MemesEntity("10", "Afsar Abbas", "http://test.com/url", "This is test data");
        MemesEntity entity2 = new MemesEntity("11", "Afsar", "http://test.com/url", "This is test data");
        MemesEntity entity3 = new MemesEntity("12", "Abbas", "http://test.com/url", "This is test data");

        List<MemesEntity> list = Arrays.asList(entity,entity2,entity3);
        
        Mockito.doReturn(list)
              .when(memesService).findAllMemes();
        
        URI uri = UriComponentsBuilder
              .fromPath("/memes")
              .build().toUri();
              
        MockHttpServletResponse response = mvc.perform(
                get(uri.toString()).accept(APPLICATION_JSON_VALUE)
            ).andReturn().getResponse();
        
        String responseJson = response.getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        List<MemesEntity> responsedata = objectMapper.readValue(responseJson, new TypeReference<List<MemesEntity>>() {});        
        assertEquals(list.size(), responsedata.size());
        Mockito.verify(memesService, Mockito.times(1)).findAllMemes();
    }
    
    
}
