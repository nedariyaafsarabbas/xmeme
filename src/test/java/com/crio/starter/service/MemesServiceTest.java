package com.crio.starter.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Optional;
import com.crio.starter.data.MemesEntity;
import com.crio.starter.repository.MemesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
public class MemesServiceTest {

    @Mock
    MemesRepository memesRepository;

    @InjectMocks
    MemesService memesService;

    @Test
    void getMemesbyId(){
        MemesEntity entity = new MemesEntity("10", "Afsar Abbas", "http://test.com/url", "This is test data");
        Mockito.doReturn(Optional.of(entity))
            .when(memesRepository).findById("10");
        Optional<MemesEntity> responseData = memesService.findById("10");

        assertEquals(entity, responseData.get());
    }
    
}
